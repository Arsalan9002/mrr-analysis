# Mrr Analysis

**mrr_simplistic.sql**
simplest form MRR consists of:
*  Activation (Prior Date is Null)
*  Recurring payments (Prior Revenue = Post Revenue)
*  Expansion (Prior Revenue < Post Revenue)
*  Contraction (Prior Revenue > Post Revenue)
*  Churn (Post Revenue is NULL)