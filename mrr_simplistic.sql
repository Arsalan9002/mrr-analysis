SELECT
    CustomerId
    , Month
    -- Extra columns for better understanding of results
    -- , PriorMonth
    -- , PriorAmount
    -- , PostMonth
    -- , PostAmount
    , (CASE
        WHEN PriorAmount IS NULL THEN 'NEW'
        WHEN PriorAmount = PostAmount THEN 'Recurring'
        WHEN PostAmount > PriorAmount THEN 'Expansion'
        WHEN PostAmount < PriorAmount THEN 'Contraction'
        WHEN PostMonth IS NULL AND PostAmount IS NULL THEN 'Churn'
    END) as UserStaus

FROM (
    SELECT
        CustomerId
        , Eventdate
        , EXTRACT(MONTH FROM Eventdate) as Month

        , LAG(EXTRACT(MONTH FROM Eventdate)) OVER 
            (PARTITION BY CustomerId ORDER BY Eventdate ASC) 
            AS PriorMonth

        , LAG(Revenue) OVER 
            (PARTITION BY CustomerId ORDER BY Eventdate ASC) 
            AS PriorAmount

        , LEAD(EXTRACT(MONTH FROM Eventdate)) OVER
            (PARTITION BY CustomerId ORDER BY Eventdate ASC)
            AS PostMonth    

        , LEAD(Revenue) OVER
            (PARTITION BY CustomerId ORDER BY Eventdate ASC)
            AS PostAmount

    FROM (

        SELECT 
            CustomerId
            , Eventdate
            , SUM(Totalvalue+Paymentamount) as Revenue
        from {{ref('stg_squezly')}}
            WHERE 
                --CustomerId='36411576' 
            AND Event IN ('purchase','subscriptionpayment')

            {{dbt_utils.group_by(n=2)}}
            ORDER BY 2 ASC    

    )
    ORDER BY Month
)





/*
Sample Data
CustomerId  |   Eventdate   |   Revenue
36411576    |   2019-06-01  |   9.99
36411576    |   2019-07-01  |   9.99
36411576    |   2019-08-01  |   9.99
36411576    |   2019-09-01  |   9.99
36411576    |   2019-10-01  |   9.99
36411576    |   2019-11-01  |   9.99
36411576    |   2019-12-01  |   0
*/